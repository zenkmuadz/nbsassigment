package com.project.moviedb.ui.popular

import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.text.style.BulletSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.project.moviedb.BuildConfig
import com.project.moviedb.R
import com.project.moviedb.datamodel.FilmModel
import com.project.moviedb.network.db.FavoriteTable
import com.project.moviedb.ui.favorite.FavoriteViewModel
import com.project.rantingservice.base.BaseActivity
import id.rupi.rupi.network.banner.FilmRequest
import id.rupi.rupi.network.film.FilmDetailResponse
import kotlinx.android.synthetic.main.activity_detail_film.*
import kotlinx.android.synthetic.main.toolbar_detail_film.*
import org.koin.android.ext.android.inject

class PopularDetailActivity : BaseActivity() {
    private val vm: PopularViewModel by inject()
    private val vmFavorite: FavoriteViewModel by inject()
    private var detail: FilmDetailResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_film)
        initToolbar()
        initView()
        initObserver()
        initRequest()
        initListener()
    }

    private fun initToolbar() {
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun initView(){

    }

    private fun initListener () {
        btnAdd.setOnClickListener {
            if (btnText.text.equals(resources.getString(R.string.remove_favorite)))
                removeFavorite()
            else
                addFavorite()
        }
    }

    private fun initObserver() {
        vm.filmDetailData.observe(this, Observer {
            detail = it
            setData(it)
            setCast()
            vmFavorite.checkFavorite(it.id)
        })
        vmFavorite.checkFavorite.observe(this, Observer {
            if (it > 0) showRemove()
            else showAdd()
        })
        vmFavorite.add.observe(this, Observer {
            vmFavorite.checkFavorite(detail!!.id)
        })
        vmFavorite.remove.observe(this, Observer {
            vmFavorite.checkFavorite(detail!!.id)
        })
    }

    private fun initRequest () {
        if (intent.hasExtra(ID_FILM)) {
            val id = intent.extras!!.getInt(ID_FILM)
            val req = FilmRequest()
            req.setAPIKEY(BuildConfig.API_KEY)
            req.setLanguage("en-US")
            vm.getDetailFilm(req, id)
        }

    }

    private fun setData(it: FilmDetailResponse) {
        showImageFromUrlWithGlide(BuildConfig.BASE_URL+it.poster_path, image)
        txtDesc.text = it.overview
        txtTitle.text = it.original_title
        var listGenre = ""
        it.genres.forEach {
            listGenre = listGenre + it.name + " " + addColorBullet("\u2022") + " "
        }
        txtGenre.text = listGenre.dropLast(2)
    }

    private fun addColorBullet(bul: String) : String {
        val spannable = SpannableString(bul)
        spannable.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.orange)),
            0, spannable.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannable.toString()
    }

    private fun addFavorite () {
        if (detail != null) {
            var genres = ""
            detail!!.genres.forEach {
                genres = genres + it.name + ", "
            }
            genres = genres.dropLast(2)
            val req = FavoriteTable(
                detail!!.id,
                detail!!.title,
                detail!!.release_date,
                genres,
                detail!!.backdrop_path)
            vmFavorite.addFavorite(req)
        }
    }

    private fun removeFavorite () {
        if (detail != null) {
            vmFavorite.removeFavoriteById(detail!!.id)
        }
    }

    private fun showAdd () {
        btnAdd.background = ActivityCompat.getDrawable(this, R.drawable.background_button_add)
        btnText.text = resources.getString(R.string.add_favorite)
        btnAdd.visibility = View.VISIBLE
        btnText.setTextColor(ActivityCompat.getColor(this, R.color.white))
    }

    private fun showRemove() {
        btnAdd.background = ActivityCompat.getDrawable(this, R.drawable.background_button)
        btnText.text = resources.getString(R.string.remove_favorite)
        btnText.setTextColor(ActivityCompat.getColor(this, R.color.black))
        imgAdd.visibility = View.GONE
    }

    private fun setCast() {
        val dummy = ArrayList<String>()
        dummy.add("Dave")
        dummy.add("Dave")
        dummy.add("Dave")
        dummy.add("Dave")
        val castAdapter = DetailCastAdapter(dummy)
        recCast.adapter = castAdapter
    }

    companion object {
        val ID_FILM = "ID_FILM"
    }

}