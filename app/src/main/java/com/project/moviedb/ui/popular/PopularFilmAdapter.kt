package com.project.moviedb.ui.popular

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.moviedb.BuildConfig
import com.project.moviedb.R
import com.project.moviedb.datamodel.FilmModel
import kotlinx.android.synthetic.main.layout_list_film.view.*
import java.util.*
import kotlin.collections.ArrayList

class PopularFilmAdapter(private val list: ArrayList<FilmModel>):
        RecyclerView.Adapter<PopularFilmAdapter.ItemViewHolder>() {

    private lateinit var context: Context
    private var listFilter: ArrayList<FilmModel> = list
    private var listener: PopularAdapterInterface? = null

    fun setInterface (callback: PopularAdapterInterface) {
        listener = callback
    }

    override fun getItemCount(): Int {
        return listFilter.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_list_film, parent, false)
        context = parent.context
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val film = listFilter[position]
        holder.txtTitle.text = film.original_title
        holder.txtDesc.text = film.overview
        showImageFromUrlWithGlide(BuildConfig.BASE_URL+film.backdrop_path, holder.ivBanner)
        holder.layMain.setOnClickListener {
            listener!!.onClickFilm(film)
        }
    }

    private fun showImageFromUrlWithGlide(link: String, imageView: ImageView) {
        val option = RequestOptions()
                .centerCrop()
                .error(R.drawable.default_image_error)

        Glide.with(context)
                .load(link)
                .apply(option)
                .into(imageView)
    }

    fun setFilter (search: String) {
        if (search.isEmpty()) {
            listFilter = list
        } else {
            var resultList = ArrayList<FilmModel>()
            list.forEach {
                if (it.original_title.contains(search, true)) {
                    resultList.add(it)
                }
            }
            listFilter = resultList
        }
        notifyDataSetChanged()
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var ivBanner = itemView.ivBanner
        internal var txtTitle = itemView.txtTitle
        internal var txtDesc = itemView.txtDesc
        internal var layMain = itemView.layMain
    }

    interface PopularAdapterInterface {
        fun onClickFilm(data: FilmModel)
    }

}
