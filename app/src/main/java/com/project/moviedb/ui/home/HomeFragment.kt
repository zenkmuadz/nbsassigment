package com.project.moviedb.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.project.moviedb.BuildConfig
import com.project.moviedb.R
import com.project.moviedb.datamodel.BannerModel
import com.project.moviedb.datamodel.FilmModel
import com.project.moviedb.ui.popular.PopularDetailActivity
import com.project.rantingservice.base.BaseFragment
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import id.rupi.rupi.network.banner.BannerRequest
import id.rupi.rupi.network.banner.FilmRequest
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : BaseFragment() {
    private val vm: HomeViewModel by inject()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRequest()
        initObserver()
    }

    private fun initRequest(){
        val req = BannerRequest()
        req.setAPIKEY(BuildConfig.API_KEY)
        req.setLanguage("en-US")
        req.setSortBy("popularity.desc")
        req.setIncludeAdult(false)
        req.setIncludeVideo(false)
        req.setPage(1)
        vm.getBannerData(req)

        val reqPopular = FilmRequest()
        reqPopular.setAPIKEY(BuildConfig.API_KEY)
        reqPopular.setLanguage("en-US")
        reqPopular.setSortBy("popularity.desc")
        reqPopular.setIncludeAdult(false)
        reqPopular.setIncludeVideo(false)
        reqPopular.setPage(1)
        vm.getPopularFilmData(reqPopular)

        val reqSoon = FilmRequest()
        reqSoon.setAPIKEY(BuildConfig.API_KEY)
        reqSoon.setLanguage("en-US")
        reqSoon.setSortBy("popularity.desc")
        reqSoon.setIncludeAdult(false)
        reqSoon.setIncludeVideo(false)
        reqSoon.setPage(1)
        reqSoon.setYear(Calendar.getInstance().get(Calendar.YEAR)+1)
        vm.getSoonFilmData(reqSoon)

    }

    private fun initObserver() {
        vm.bannerData.observe(requireActivity(), Observer {
            setBanner(it)
        })
        vm.filmSoonData.observe(requireActivity(), Observer {
            setSoon(it)
        })
        vm.filmPopularData.observe(requireActivity(), Observer {
            setPopular(it)
        })
    }

    private fun setBanner(banner: ArrayList<BannerModel>) {
        val adapter = BannerAdapter(banner)

        imageSlider.setSliderAdapter(adapter)
        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH)
        imageSlider.setScrollTimeInSec(4) //set scroll delay in seconds :
        imageSlider.startAutoCycle()
        adapter.setInterface(object: BannerAdapter.BannerAdapterInterface{
            override fun onClickFilm(data: BannerModel) {
                goToDetail(data.id)
            }

        })
    }

    private fun setPopular(list: ArrayList<FilmModel>) {
        val showList = ArrayList<FilmModel>()
        if(list.size > 10) {
            for (i in 0..9) {
                showList.add(list.get(i))
            }
        } else {
            showList.addAll(list)
        }

        val adapter = HomeFilmAdapter(showList)
        recPopular.adapter = adapter
        adapter.setInterface(object: HomeFilmAdapter.HomeFilmAdapterInterface{
            override fun onClickFilm(data: FilmModel) {
                goToDetail(data.id)
            }
        })
    }

    private fun setSoon(list: ArrayList<FilmModel>) {
        val showList = ArrayList<FilmModel>()
        if(list.size > 10) {
            for (i in 0..9) {
                showList.add(list.get(i))
            }
        } else {
            showList.addAll(list)
        }

        val adapter = HomeFilmAdapter(showList)
        recSoon.adapter = adapter
        adapter.setInterface(object: HomeFilmAdapter.HomeFilmAdapterInterface{
            override fun onClickFilm(data: FilmModel) {
                goToDetail(data.id)
            }
        })
    }

    private fun goToDetail(id: Int) {
        val i = Intent(requireContext(), PopularDetailActivity::class.java)
        i.putExtra(PopularDetailActivity.ID_FILM, id)
        startActivity(i)
    }
}