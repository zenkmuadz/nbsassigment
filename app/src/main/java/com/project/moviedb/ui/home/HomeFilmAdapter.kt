package com.project.moviedb.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.moviedb.BuildConfig
import com.project.moviedb.R
import com.project.moviedb.datamodel.FilmModel
import kotlinx.android.synthetic.main.layout_list_home.view.*

class HomeFilmAdapter(private val list: MutableList<FilmModel>):
        RecyclerView.Adapter<HomeFilmAdapter.ItemViewHolder>() {

    private lateinit var context: Context
    private var listener: HomeFilmAdapterInterface? = null

    fun setInterface(callback: HomeFilmAdapterInterface) {
        listener = callback
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_list_home, parent, false)
        context = parent.context
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        showImageFromUrlWithGlide(BuildConfig.BASE_URL+list[position].backdrop_path, holder.ivBanner)
        holder.layMain.setOnClickListener {
            listener!!.onClickFilm(list[position])
        }
    }

    private fun showImageFromUrlWithGlide(link: String, imageView: ImageView) {
        val option = RequestOptions()
                .centerCrop()
                .error(R.drawable.default_image_error)

        Glide.with(context)
                .load(link)
                .apply(option)
                .into(imageView)
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var ivBanner = itemView.ivBanner
        internal var layMain = itemView.layMain
    }

    interface HomeFilmAdapterInterface {
        fun onClickFilm(data: FilmModel)
    }

}
