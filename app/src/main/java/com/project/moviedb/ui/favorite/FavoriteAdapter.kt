package com.project.moviedb.ui.favorite

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.moviedb.BuildConfig
import com.project.moviedb.R
import com.project.moviedb.datamodel.FilmModel
import com.project.moviedb.network.db.FavoriteTable
import com.project.moviedb.utils.DateFormat
import kotlinx.android.synthetic.main.layout_list_favorite.view.*
import java.util.*
import kotlin.collections.ArrayList

class FavoriteAdapter(private val list: MutableList<FavoriteTable>):
        RecyclerView.Adapter<FavoriteAdapter.ItemViewHolder>() {

    private lateinit var context: Context
    private var listFilter: MutableList<FavoriteTable> = list
    private var listener: FavoriteAdapterInterface? = null

    fun setInterface (callback: FavoriteAdapterInterface) {
        listener = callback
    }

    override fun getItemCount(): Int {
        return listFilter.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_list_favorite, parent, false)
        context = parent.context
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val film = listFilter[position]
        println(film.id)
        holder.txtTitle.text = film.title
        holder.txtYear.text = DateFormat.convertToNoZone(film.release_date, DateFormat.YEAR)
        holder.txtGenre.text = film.genres
        showImageFromUrlWithGlide(BuildConfig.BASE_URL+film.backdrop_path, holder.ivBanner)
        holder.layMain.setOnClickListener {
            listener!!.onClickFilm(film)
        }
        holder.imgLove.setOnClickListener {
            listener!!.onDeleteFilm(film)
        }
    }

    private fun showImageFromUrlWithGlide(link: String, imageView: ImageView) {
        val option = RequestOptions()
                .centerCrop()
                .error(R.drawable.default_image_error)

        Glide.with(context)
                .load(link)
                .apply(option)
                .into(imageView)
    }

    fun setFilter (search: String) {
        if (search.isEmpty()) {
            listFilter = list
        } else {
            var resultList = ArrayList<FavoriteTable>()
            list.forEach {
                if (it.title.contains(search, true)) {
                    resultList.add(it)
                }
            }
            listFilter = resultList
        }
        notifyDataSetChanged()
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var ivBanner = itemView.ivBanner
        internal var txtTitle = itemView.txtTitle
        internal var txtGenre = itemView.txtGenre
        internal var txtYear = itemView.txtYear
        internal var layMain = itemView.layMain
        internal var imgLove = itemView.love
    }

    interface FavoriteAdapterInterface {
        fun onClickFilm(data: FavoriteTable)
        fun onDeleteFilm(data: FavoriteTable)
    }

}
