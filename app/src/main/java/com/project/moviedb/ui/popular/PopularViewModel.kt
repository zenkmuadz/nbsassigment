package com.project.moviedb.ui.popular

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.project.moviedb.BuildConfig
import com.project.moviedb.datamodel.BannerModel
import com.project.moviedb.datamodel.FilmModel
import com.project.rantingservice.base.BaseViewModel
import id.rupi.rupi.network.banner.*
import id.rupi.rupi.network.film.FilmDetailResponse
import id.rupi.rupi.network.film.FilmResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PopularViewModel(
    private val filmRepository: FilmRepository
) : BaseViewModel() {

    var filmPopularData = MutableLiveData<ArrayList<FilmModel>>()
    var filmDetailData = MutableLiveData<FilmDetailResponse>()

    fun getPopularFilmData(req: FilmRequest) {
        viewModelScope.launch {
            try {
                var response: FilmResponse? = null

                withContext(Dispatchers.IO) {
                    response = filmRepository.getFilmPopular(
                        req
                    )
                }
                filmPopularData.value = response?.results

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getDetailFilm(req: FilmRequest, movie_id:Int) {
        viewModelScope.launch {
            try {
                var response: FilmDetailResponse? = null

                withContext(Dispatchers.IO) {
                    response = filmRepository.getDetailFilm(
                        req, movie_id
                    )
                }
                filmDetailData.value = response!!

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}