package com.project.moviedb.ui.favorite

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.project.moviedb.BuildConfig
import com.project.moviedb.datamodel.BannerModel
import com.project.moviedb.datamodel.FilmModel
import com.project.moviedb.network.db.FavoriteTable
import com.project.moviedb.network.repository.favorite.FavoriteRepository
import com.project.moviedb.utils.Coroutines
import com.project.moviedb.utils.lazyDeferred
import com.project.rantingservice.base.BaseViewModel
import id.rupi.rupi.network.banner.*
import id.rupi.rupi.network.film.FilmDetailResponse
import id.rupi.rupi.network.film.FilmResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavoriteViewModel(
    private val favoriteRepository: FavoriteRepository
) : BaseViewModel() {
    var favorite = MutableLiveData<List<FavoriteTable>>()
    var favoriteOne = MutableLiveData<List<FavoriteTable>>()
    var checkFavorite = MutableLiveData<Int>()
    var remove = MutableLiveData<Boolean>()
    var add = MutableLiveData<Boolean>()

    fun addFavorite(item: FavoriteTable){
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    favoriteRepository.insertFavorite(
                        item
                    )
                }
                add.value = true
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun removeFavorite(item: FavoriteTable){
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    favoriteRepository.deleteFavorite(
                        item
                    )
                }
                remove.value = true
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun removeFavoriteById(id: Int){
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    favoriteRepository.deleteFavoriteByID(
                            id
                    )
                }
                remove.value = true
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun checkFavorite(id: Int){
        viewModelScope.launch {
            try {
                var count = 0
                withContext(Dispatchers.IO) {
                    count = favoriteRepository.checkFavorite(
                        id
                    )
                }
                checkFavorite.value = count
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getFavorite() {
        viewModelScope.launch {
            try {
                var data: List<FavoriteTable> = ArrayList()
                withContext(Dispatchers.IO) {
                    data = favoriteRepository.getFavorite()
                }
                favorite.value = data

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getFavoriteByID(id: Int) {
        viewModelScope.launch {
            try {
                var data: List<FavoriteTable> = ArrayList()
                withContext(Dispatchers.IO) {
                    data = favoriteRepository.getFavoriteByID(id)
                }
                favoriteOne.value = data

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun deleteAll() {
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    favoriteRepository.deleteAll()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }



}