package com.project.moviedb.ui.popular

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import com.project.moviedb.BuildConfig
import com.project.moviedb.R
import com.project.moviedb.datamodel.FilmModel
import com.project.moviedb.ui.popular.PopularDetailActivity.Companion.ID_FILM
import com.project.rantingservice.base.BaseFragment
import id.rupi.rupi.network.banner.FilmRequest
import kotlinx.android.synthetic.main.fragment_popular.*
import org.koin.android.ext.android.inject


class PopularFragment : BaseFragment() {
    private val vm: PopularViewModel by inject()
    private var adapter: PopularFilmAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_popular, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFilm()
        initObserver()
        initListener()
    }

    private fun initFilm() {
        val req = FilmRequest()
        req.setAPIKEY(BuildConfig.API_KEY)
        req.setLanguage("en-US")
        req.setSortBy("popularity.desc")
        req.setIncludeAdult(false)
        req.setIncludeVideo(false)
        req.setPage(1)
        vm.getPopularFilmData(req)
    }

    private fun initObserver() {
        vm.filmPopularData.observe(requireActivity(), Observer {
            setPopular(it)
        })
    }

    private fun initListener() {
        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(s: Editable) {
                adapter!!.setFilter(s.toString())
                if (!s.trim().isEmpty()) showQueryText(s.toString())
                else hideQueryText()
            }
        })
    }

    private fun setPopular(showList: ArrayList<FilmModel>) {
        adapter = PopularFilmAdapter(showList)
        recPopular.adapter = adapter
        adapter!!.setInterface(object: PopularFilmAdapter.PopularAdapterInterface {
            override fun onClickFilm(data: FilmModel) {
                val i = Intent(requireContext(), PopularDetailActivity::class.java)
                i.putExtra(ID_FILM, data.id)
                startActivity(i)
            }

        })
    }

    private fun showQueryText(search: String) {
        txtSearch.text = "'${search}'"
        groupSearch.isGone = false
    }

    private fun hideQueryText () {
        groupSearch.isGone = true
    }
}