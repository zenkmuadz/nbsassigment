package com.project.moviedb.ui.favorite

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.SearchView
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import com.project.moviedb.BuildConfig
import com.project.moviedb.R
import com.project.moviedb.datamodel.FilmModel
import com.project.moviedb.network.db.FavoriteTable
import com.project.moviedb.ui.popular.PopularDetailActivity
import com.project.moviedb.ui.popular.PopularDetailActivity.Companion.ID_FILM
import com.project.rantingservice.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.koin.android.ext.android.inject


class FavoriteFragment : BaseFragment() {
    private val vm: FavoriteViewModel by inject()
    private var adapter: FavoriteAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_favorite, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFavorite()
        initObserver()
        initListener()
    }

    private fun initFavorite() {
        vm.getFavorite()
    }

    private fun initObserver() {
        vm.favorite.observe(requireActivity(), Observer {
            setFavorite(it.toMutableList())
        })
        vm.remove.observe(requireActivity(), Observer {
            initFavorite()
        })
    }

    private fun initListener() {
        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(s: Editable) {
                adapter!!.setFilter(s.toString())
                if (!s.trim().isEmpty()) showQueryText(s.toString())
                else hideQueryText()
            }
        })
    }

    private fun setFavorite(showList: MutableList<FavoriteTable>) {
        adapter = FavoriteAdapter(showList)
        recPopular.adapter = adapter
        adapter!!.setInterface(object: FavoriteAdapter.FavoriteAdapterInterface {
            override fun onClickFilm(data: FavoriteTable) {
                val i = Intent(requireContext(), PopularDetailActivity::class.java)
                i.putExtra(ID_FILM, data.id)
                startActivityForResult(i, 1)
            }

            override fun onDeleteFilm(data: FavoriteTable) {
                showDialogDelete(data)
            }

        })
    }

    private fun showDialogDelete(item: FavoriteTable) {
        val alertDialogExitBuilder = AlertDialog.Builder(ContextThemeWrapper(requireContext(), R.style.AlertDialogTheme))
        alertDialogExitBuilder.setMessage("Are you sure for delete this favorite ?")
        alertDialogExitBuilder.setPositiveButton("YA") { dialog, which ->
            vm.removeFavorite(item)
        }
        alertDialogExitBuilder.setNegativeButton("TIDAK", null)
        alertDialogExitBuilder.show()
    }

    private fun showQueryText(search: String) {
        txtSearch.text = "'${search}'"
        groupSearch.isGone = false
    }

    private fun hideQueryText () {
        groupSearch.isGone = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        initFavorite()
    }
}