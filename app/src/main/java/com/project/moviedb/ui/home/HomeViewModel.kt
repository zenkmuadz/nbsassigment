package com.project.moviedb.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.project.moviedb.BuildConfig
import com.project.moviedb.datamodel.BannerModel
import com.project.moviedb.datamodel.FilmModel
import com.project.rantingservice.base.BaseViewModel
import id.rupi.rupi.network.banner.*
import id.rupi.rupi.network.film.FilmResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(
    private val bannerRepository: BannerRepository,
    private val filmRepository: FilmRepository
) : BaseViewModel() {

    var bannerData = MutableLiveData<ArrayList<BannerModel>>()
    var filmSoonData = MutableLiveData<ArrayList<FilmModel>>()
    var filmPopularData = MutableLiveData<ArrayList<FilmModel>>()

    fun getBannerData(req: BannerRequest) {
        viewModelScope.launch {
            try {
                var response: BannerResponse? = null

                withContext(Dispatchers.IO) {
                    response = bannerRepository.getBanner(
                        req
                    )
                }
                bannerData.value = response?.results

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getSoonFilmData(req: FilmRequest) {
        viewModelScope.launch {
            try {
                var response: FilmResponse? = null

                withContext(Dispatchers.IO) {

                    response = filmRepository.getFilmSoon(
                        req
                    )
                }
                filmSoonData.value = response?.results

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getPopularFilmData(req: FilmRequest) {
        viewModelScope.launch {
            try {
                var response: FilmResponse? = null

                withContext(Dispatchers.IO) {
                    response = filmRepository.getFilmPopular(
                        req
                    )
                }
                filmPopularData.value = response?.results

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}