package com.project.moviedb

import androidx.multidex.MultiDexApplication
import com.project.moviedb.di.networkModule
import com.project.moviedb.di.roomDBModule
import com.project.rantingservice.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.util.*

class MainApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {

            androidContext(this@MainApplication)
            //CoreComponent
            modules(
                    networkModule,
                    viewModelModule,
                    roomDBModule
            )
        }
    }
}
