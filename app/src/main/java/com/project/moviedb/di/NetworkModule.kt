package com.project.moviedb.di

import com.project.moviedb.network.connection.NetworkClient
import com.project.moviedb.network.connection.NetworkConnectionInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module

val networkModule = module {

    single { NetworkConnectionInterceptor(get()) }

    single(named(RESTAPI_SERVICE)) { NetworkClient.serviceNetworkAPI(get()) }
}

const val RESTAPI_SERVICE = "RESTAPI_SERVICE"



