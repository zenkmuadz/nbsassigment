package com.project.moviedb.di

import com.project.moviedb.network.db.DatabaseRequestManagers
import org.koin.dsl.module

val roomDBModule = module {
    single { DatabaseRequestManagers(get()) }
}