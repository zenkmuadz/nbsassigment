package com.project.rantingservice.di

import com.project.moviedb.di.RESTAPI_SERVICE
import com.project.moviedb.network.repository.favorite.FavoriteRepository
import com.project.moviedb.ui.favorite.FavoriteViewModel
import com.project.moviedb.ui.home.HomeViewModel
import com.project.moviedb.ui.popular.PopularViewModel
import id.rupi.rupi.network.banner.BannerRepository
import id.rupi.rupi.network.banner.BannerService
import id.rupi.rupi.network.banner.FilmRepository
import id.rupi.rupi.network.banner.FilmService
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val viewModelModule = module {

    factory { BannerRepository(get<Retrofit>(named(RESTAPI_SERVICE)).create(BannerService::class.java)) }
    factory { FilmRepository(get<Retrofit>(named(RESTAPI_SERVICE)).create(FilmService::class.java)) }
    factory { FavoriteRepository(get()) }

    viewModel { HomeViewModel(get(), get()) }
    viewModel { PopularViewModel(get()) }
    viewModel { FavoriteViewModel(get()) }
}