package id.rupi.rupi.network.film

import com.project.moviedb.datamodel.FilmModel
import com.project.moviedb.datamodel.GenreModel
import com.project.rantingservice.base.BaseResponse

class FilmDetailResponse : BaseResponse() {
    val backdrop_path: String = ""
    val genres: ArrayList<GenreModel> = ArrayList()
    val id: Int = 0
    val original_title: String = ""
    val title: String = ""
    val overview: String = ""
    val poster_path: String = ""
    val release_date: String = ""
}