package id.rupi.rupi.network.banner

import id.rupi.rupi.network.film.FilmDetailResponse
import id.rupi.rupi.network.film.FilmResponse

class FilmRepository(private val filmService: FilmService) {

    suspend fun getFilmSoon(req: FilmRequest): FilmResponse =
        filmService.getFilm(req)

    suspend fun getFilmPopular(req: FilmRequest): FilmResponse =
        filmService.getFilm(req)

    suspend fun getDetailFilm(req: FilmRequest, movie_id: Int): FilmDetailResponse =
        filmService.getFilmDetail(movie_id, req)

}