package id.rupi.rupi.network.banner

import com.project.moviedb.datamodel.BannerModel
import com.project.rantingservice.base.BaseResponse

class BannerResponse : BaseResponse() {
    val results: ArrayList<BannerModel> = ArrayList()
}