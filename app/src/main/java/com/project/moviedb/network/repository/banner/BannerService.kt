package id.rupi.rupi.network.banner

import retrofit2.http.*

interface BannerService {

    @GET("/3/discover/movie")
    suspend fun getBanner(
        @QueryMap parameter: BannerRequest
    ): BannerResponse
}