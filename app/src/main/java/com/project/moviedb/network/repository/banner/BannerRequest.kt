package id.rupi.rupi.network.banner

class BannerRequest() : HashMap<String, Any>() {

    fun setAPIKEY (api_key: String) {
        put("api_key", api_key)
    }
    fun setLanguage (language: String) {
        put("language", language)
    }
    fun setSortBy (sort_by: String) {
        put("sort_by", sort_by)
    }
    fun setIncludeAdult (include_adult: Boolean) {
        put("include_adult", include_adult)
    }
    fun setIncludeVideo (include_video: Boolean) {
        put("include_video", include_video)
    }
    fun setPage (page: Int) {
        put("page", page)
    }
}
