package id.rupi.rupi.network.film

import com.project.moviedb.datamodel.FilmModel
import com.project.rantingservice.base.BaseResponse

class FilmResponse : BaseResponse() {
    val results: ArrayList<FilmModel> = ArrayList()
}