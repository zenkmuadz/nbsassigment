package id.rupi.rupi.network.banner

class BannerRepository(private val bannerService: BannerService) {

    suspend fun getBanner(bannerRequest: BannerRequest): BannerResponse =
        bannerService.getBanner(bannerRequest)

}