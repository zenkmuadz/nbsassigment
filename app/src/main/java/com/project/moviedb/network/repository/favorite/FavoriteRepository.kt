package com.project.moviedb.network.repository.favorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.project.moviedb.network.db.DatabaseRequestManagers
import com.project.moviedb.network.db.FavoriteTable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FavoriteRepository(private val room: DatabaseRequestManagers) {

//    suspend fun getFavorite(): LiveData<MutableList<FavoriteTable>> {
//        return withContext(Dispatchers.IO) {
//            room.getDaoFavorite().getList()
//        }
//    }

    fun getFavorite(): List<FavoriteTable> {
        return room.getDaoFavorite().getList()

    }

    fun checkFavorite(id: Int): Int {
        return room.getDaoFavorite().getRowCount(id)

    }

    suspend fun insertFavorite(item: FavoriteTable) {
        return room.getDaoFavorite().insert(item)
    }

    suspend fun deleteFavorite(item: FavoriteTable){
        return room.getDaoFavorite().delete(item)
    }

    suspend fun deleteFavoriteByID(id: Int){
        return room.getDaoFavorite().deleteById(id)
    }

    fun getFavoriteByID(id: Int): List<FavoriteTable> {
        return room.getDaoFavorite().getListByID(id)

    }

    fun deleteAll(){
        return room.getDaoFavorite().deleteAll()
    }
}