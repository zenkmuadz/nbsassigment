package com.project.moviedb.network.db

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class FavoriteTable(
    @PrimaryKey
    val id: Int,

    val title: String,

    val release_date: String,

    val genres: String,

    val backdrop_path: String

) : Parcelable {

    @Dao
    interface DataAccessObject {
        @Insert
        suspend fun insert(item: FavoriteTable)

        @Update
        suspend fun update(item: FavoriteTable)

        @Delete
        suspend fun delete(item: FavoriteTable)

        @Transaction
        @Query("SELECT * FROM FavoriteTable")
        fun getList(): List<FavoriteTable>

        @Query("SELECT * FROM FavoriteTable WHERE id = :id")
        fun getListByID(id: Int): List<FavoriteTable>

        @Query("SELECT COUNT(id) FROM FavoriteTable WHERE id = :id")
        fun getRowCount(id: Int): Int

        @Query("DELETE FROM FavoriteTable WHERE id = :id")
        suspend fun deleteById(id: Int)

        @Query("DELETE FROM FavoriteTable")
        fun deleteAll()
    }
}