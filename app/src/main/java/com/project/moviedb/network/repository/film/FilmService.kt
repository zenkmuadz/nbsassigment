package id.rupi.rupi.network.banner

import id.rupi.rupi.network.film.FilmDetailResponse
import id.rupi.rupi.network.film.FilmResponse
import retrofit2.http.*

interface FilmService {
    @GET("/3/discover/movie")
    suspend fun getFilm(
        @QueryMap parameter: FilmRequest
    ): FilmResponse

    @GET("3/movie/{movie_id}")
    suspend fun getFilmDetail(
        @Path("movie_id") movie_id: Int,
        @QueryMap parameter: FilmRequest
    ): FilmDetailResponse
}