package com.project.rantingservice.base

abstract class BaseResponse {
    var status_code: Int = 0
    var status_message: String = ""
    var success: Boolean = false
}