import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

fun main() {
	//tes valid string
	println(isValidString("aabbccddeefghi"))
	println(isValidString("abcdefghhgfedecba"))
	println(isValidString("abcbdcd"))

	//tes rotate left
	rotateLeft(intArrayOf(2, 3, 5, 1, 2, 3, 9, 8), 5)

	//test push ranked
	println(getLatestRank(arrayOf(200, 150, 150, 90), arrayOf(80, 90, 210)).joinToString())
	
	//test magic square
	makeMagicSquare()
}

//tes valid string
fun isValidString(input: String) : Boolean {
	val dataChar = HashMap<Char, Int>()
	for (i in input.toCharArray()) {
		if (dataChar.keys.contains(i)) {
			var countChar = dataChar[i]
			dataChar[i] = countChar!! + 1
		} else {
			dataChar[i] = 1
		}
	}
	var data = ArrayList<Int>()
	for (c in dataChar.keys) {
		data.add(dataChar[c]!!)
	}
	val dataDifference = data.groupingBy {it}.eachCount().values

	if (data.groupingBy {it}.eachCount().count() > 2) return false
	if (dataDifference.max() != 1 && dataDifference.min() != 1) return false

	return true
}

//tes rotate left
fun rotateLeft(arrayInt: IntArray, numOfRotation: Int) {
	val list = arrayInt.toList()
	Collections.rotate(list, numOfRotation * -1)
	println(list.toIntArray().joinToString())
}

//test push ranked
fun getLatestRank(ranked: Array<Int>, player: Array<Int>): Array<Int> {
	val rankedOutput = ArrayList<Int>()
	player.forEach {
		rankedOutput.add(
			ranked.plus(it)
				.distinct()
				.sorted()
				.asReversed()
				.indexOf(it) + 1
		)
	}
	println(ranked.joinToString())
	return rankedOutput.toTypedArray()
}

//test magic square
fun makeMagicSquare () {
	val square = arrayOf(
		intArrayOf(3, 1, 4),
		intArrayOf(4, 2, 7),
		intArrayOf(2, 5, 6)
	)
	var sum = ArrayList<Int>()
	var total = 0
	square.forEach { i ->
		i.forEach {
			total = total + it
		}
		sum.add(total)
		total = 0
	}
	println(sum.joinToString())
	total = 0
	for (i in 0..square.size-1) {
		for (j in 0..square.get(i).size-1) {
			total = total + square[j][i]
		}
		sum.add(total)
		total = 0
	}
	val sumSquare = sum.max()

	println(sumSquare)
	findMagicSquare (3, sumSquare!!)
}

fun findMagicSquare (n: Int, sum: Int) {
	val magicSquare = Array(n) {
		IntArray(
			n
		)
	}

	var number = 1
	var row = 0
	var column = n / 2
	var curr_row: Int
	var curr_col: Int
	while (number <= n * n) {
		magicSquare[row][column] = number
		number++
		curr_row = row
		curr_col = column
		row -= 1
		column += 1
		if (row == -1) {
			row = n - 1
		}
		if (column == n) {
			column = 0
		}
		if (magicSquare[row][column] != 0) {
			row = curr_row + 1
			column = curr_col
			if (row == -1) {
				row = n - 1
			}
		}
	}

	for (i in magicSquare.indices) {
		for (j in magicSquare.indices) {
			print(magicSquare[i][j].toString() + " ")
		}
		println()
	}
}